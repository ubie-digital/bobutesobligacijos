<?php
/**
 * The template for displaying the footer.
 *
 */
?>
<footer id="colophon" class="site-footer" role="contentinfo">

<?php $footer_section_choice = of_get_option( 'footer_section_choice' ); if ( $footer_section_choice == '1' ) { ?>
<?php } ?>

	<div class="copyright_wrap">
		<div class="row">

				<div class="large-12 columns">

					<div class="site-info">
						<span>&#169; <?php _e('Copyright','rescue'); ?> <?php echo date('Y '); ?><?php echo of_get_option( 'copyright_text' ); ?> Sukurta: <a href="http://mediainovacijos.lt" target="_blank">Media Inovacijos</a></span>
					</div><!-- .site-info -->

				</div><!-- .large-12 -->

		</div><!-- .row -->
	</div><!-- .copyright_wrap -->

</footer><!-- .site-footer #colophon -->

<?php wp_footer(); ?>
<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-P67VXB"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-P67VXB');</script>
<!-- End Google Tag Manager -->
<script type="text/javascript" src="http://sanetit.tradedoubler.com/anet-631697268?type(js)loc(138521)g(22232644)" charset="ISO-8859-1"></script>
</body>
</html>