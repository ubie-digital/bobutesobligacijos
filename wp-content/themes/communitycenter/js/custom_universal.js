/* ----------------- Start Document ----------------- */
(function($){
	$(document).ready(function(){
		'use strict';

            if(window.location.href.indexOf("thanks") > -1) {
                $('#pressMeDuk').click();
            }

            if(window.location.href.indexOf("failed") > -1) {
                $('#pressMeDuk').click();
            }

        var footeris = $('.site-footer');
        footeris.hide();
        if($(document).height() < $(window).height()){
            footeris.show();
        }
        $(window).resize(function(){
            if($(document).height() > $(window).height()){
                footeris.slideUp('slow');
            }
            else{
                footeris.slideDown('slow');
            }
        });


        $(window).scroll(function(){
        console.log($(window).scrollTop(), $(window).height(), $(document).height());

            if ($(window).scrollTop() + $(window).height() >= $(document).height() - 0)
            {

                footeris.slideDown('slow');
                $('#white2').stop().animate({
                    bottom:'6px'
                },400);
            }
            else
            {
                footeris.slideUp('slow');
                $('#white2').stop().animate({
                    bottom:'-44px'
                },400);
            }
        });

        if ($(window).height() >= $(document).height() )
        {
            footeris.data('size','hide');
        }
        else
        {
            footeris.data('size','show');
        }

        $('.menuButton').hover(function () {
            var imgButtonDiv = $( this).find(".buttonImg");
            var imgButton = imgButtonDiv.attr('src');
            if (imgButton.indexOf("_red") >= 0) {
               imgButton = imgButton.slice(0,-8);
               imgButton = imgButton + '.png';
            } else {
                imgButton = imgButton.slice(0,-4);
                imgButton = imgButton + '_red.png';
            }
            imgButtonDiv.attr('src', imgButton);
        });


/*----------------------------------------------------*/
/*	Foundation Magic
/*----------------------------------------------------*/

	$(document).foundation();

/* ------------------ End Document ------------------ */
});
	
})(this.jQuery);