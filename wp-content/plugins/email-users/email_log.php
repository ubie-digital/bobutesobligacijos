<?php
/* vim: set expandtab tabstop=4 shiftwidth=4: */
/*  Copyright 2012 Nerijus Dredas

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
?>

<?php
if (!current_user_can(MAILUSERS_EMAIL_SINGLE_USER_CAP)) {
    wp_die(printf('<div class="error fade"><p>%s</p></div>',
        __('Negalite stebėti sms išrašų.', MAILUSERS_I18N_DOMAIN)));
}
global $wpdb, $wp_query;

$query = "SELECT * FROM bob_email_log ORDER BY id DESC";

$total_record = count($wpdb->get_results($query));
$paged = isset($_GET['pagenum']) ? absint($_GET['pagenum']) : 1;
$post_per_page = get_option('posts_per_page');
$offset = ($paged - 1) * $post_per_page;
$max_num_pages = ceil($total_record / $post_per_page);

$wp_query->found_posts = $total_record;
$wp_query->max_num_pages = $max_num_pages;
$limit_query = " LIMIT " . $post_per_page . " OFFSET " . $offset;
$result = $wpdb->get_results($query . $limit_query, OBJECT); // return OBJECT

?>
<div class="wrap">
    <table id="codes_table" class="widefat plugins datatables">
        <thead>
        <tr>
            <th scope="col" width="100">ID</th>
            <th scope="col" width="250">Gavėjas</th>
            <th scope="col" width="750">Žinutė</th>
            <th scope="col">Data</th>
        </tr>
        </thead>
        <tfoot>
        <tr>
            <th scope="col">ID</th>
            <th scope="col">Gavėjas</th>
            <th scope="col">Žinutė</th>
            <th scope="col">Data</th>
        </tr>
        </tfoot>
        <tbody class="codes_table">
        <?php
        if ($result) {
            foreach ($result as $email) {
                echo '<tr class="token">';
                echo '<td><div class="activation">' . $email->id . '</div></td>';
                if ($email->userID) {
                echo '<td><div class="activation"><a href="' . admin_url() . 'user-edit.php?user_id=' . $email->userID . '">' . $email->email . '</a></div></td>';
                } else {
                    echo '<td><div class="activation">' . $email->email . '</div></td>';
                }
                echo '<td><div class="activation">' . $email->message . '</div></td>';
                echo '<td><div class="activation">' . $email->date . '</div></td>';
                echo '</tr>';
            }
        }
        ?>
        </tbody>
    </table>
    <?php
    $page_links = paginate_links(array(
        'base' => add_query_arg('pagenum', '%#%'),
        'format' => '',
        'prev_text' => __('&laquo;', 'aag'),
        'next_text' => __('&raquo;', 'aag'),
        'total' => $max_num_pages,
        'current' => $paged
    ));

    if ($page_links) {
        echo '<div class="tablenav"><div class="tablenav-pages" style="margin: 1em 0">' . $page_links . '</div></div>';
    }
    ?>
</div>