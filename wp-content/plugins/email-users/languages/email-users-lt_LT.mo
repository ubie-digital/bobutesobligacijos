��    H      \  a   �         �  !  ^   �  2     6   ?     v     �     �     �     �     �  .   �     	     	  
   1	     <	     K	     Y	     a	     d	  >   v	     �	     �	  "   �	  !   �	     
  !   +
      M
     n
     �
  >   �
  <   �
     $     (  
   +  8   6  
   o     z  +   �  )   �  
   �     �     �  
   �     �          %  !   :  !   \  
   ~     �     �     �     �  I   �  .        2     7     E     U     ^  #   b  2   �  8   �  �   �  c   �  J        N  $   h     �      �  !   �  �  �  �  �  $   y  "   �  2   �     �               %     9  +   E     q     �     �     �     �     �     �     �     �       
   ,     7  #   L  )   p     �  )   �  "   �     �       '   !     I     i     l     q  ,   �     �  
   �     �  "   �     �                         9  $   V  '   {  $   �  
   �     �     �  
   �        5        ;  
   [     f  
   |     �     �  $   �  3   �  1   �  J   +  ,   v  C   �     �     �       #   "     F                                    A   B   G       /   %      5           F   >   @   
   C       	      0   $          (   ;   #       !         *   E                                          <   2      3   '             9   "   ?       7   1            )              H              8       4   6   &   :   ,                    +       .   D   -      =       <p>Hello, </p><p>I would like to bring your attention on a new post published on the blog. Details of the post follow; I hope you will find it interesting.</p><p>Best regards, </p><p>%FROM_NAME%</p><hr><p><strong>%POST_TITLE%</strong></p><p>%POST_EXCERPT%</p><ul><li>Link to the post: <a href="%POST_URL%">%POST_URL%</a></li><li>Link to %BLOG_NAME%: <a href="%BLOG_URL%">%BLOG_URL%</a></li></ul> Accept to receive emails sent to multiple recipients (but still accept emails sent only to me) Accept to receive post or page notification emails Allow Users to control their own Email Users settings: Default User Settings Display Name E-Mail Address Email Preferences Email Users Email sent to %s user(s). Filter Users with no role from Recipient List: Mail format Mail will be sent as: Mass Email Mass Email Off Mass Email On Message No No users selected Notification Mail Preview (based on default notification body) Notifications Notifications Off Notifications Off & Mass Email Off Notifications Off & Mass Email On Notifications On Notifications On & Mass Email Off Notifications On & Mass Email On Notify Users About this Page Notify Users About this Post Number of Users who accept emails sent to multiple recipients: Number of Users who accept post or page notification emails: Off On Plain text Please correct the errors displayed above and try again. Powered by Preview Receive emails sent to multiple recipients: Receive post or page notification emails: Recipients Role Search Send Email Send Group Message Send Individual Message Send a Group Message Send an Email to Individual Users Send an Email to a Group of Users Send to %s Send to Group(s) Send to User(s) Sender Subject The users that did not agree to receive notifications do not appear here. Use CTRL key to select/deselect multiple items User User Settings User Statistics Username Yes You are not allowed to send emails. You are only allowed to select one user at a time. You can select multiple groups by pressing the CTRL key. You can select multiple users by pressing the CTRL key.  When selecting multiple users, any user who should not receive Mass Email will be filtered from the recipient list. You can send an email to one or more user groups (i.e. users belonging to the same WordPress role). You can send an email to one or more users by selecting them individually. You must enter a subject. You must enter at least a recipient. You must enter some content. You must select at least a role. You must specify the mail format. Project-Id-Version: Email-Users
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2014-09-13 21:23-0000
PO-Revision-Date: 2014-10-14 11:45+0300
Last-Translator: Mike Walsh <mpwalsh8@gmail.com>
Language-Team: 
Language: en_US
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.5.4
X-Poedit-KeywordsList: _;gettext;gettext_noop;__;_e
X-Poedit-Basepath: .
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-SourceCharset: UTF-8
X-Poedit-SearchPath-0: ..
 <p>Hello, </p><p>I would like to bring your attention on a new post published on the blog. Details of the post follow; I hope you will find it interesting.</p><p>Best regards, </p><p>%FROM_NAME%</p><hr><p><strong>%POST_TITLE%</strong></p><p>%POST_EXCERPT%</p><ul><li>Nuoroda į įrašą: <a href="%POST_URL%">%POST_URL%</a></li><li>Nuoroda į %BLOG_NAME%: <a href="%BLOG_URL%">%BLOG_URL%</a></li></ul> Gauti laiškus išsiųstus masiškai Gauti laiškus siųstus vartotojui Leisti vartotojams patiems kontroliuoti nustatymus Numatytieji nustatymai Vardas El. pašto adresas Laiškų nustatymai Pranešimai El. laiškas išsiųstas %s vartotojui(ų). Filtruoti vartotojus be rolės Formatas Laiškas bus išsiųstas kaip: Prenumerata Prenumerata išjungta Prenumerata įjungta Žinutė Ne Nepasitinkta vartotojų Šitaip atrodis laiškas Ispėjimai Ispėjimai išjungti Ispėjimai ir prenumerata išjungti Ispėjimai išjungti prenumerata įjungta Ispėjimai įjungti Ispėjimai įjungti prenumerata išjungta Ispėjimai ir prenumerata įjungti Ispėti vartotojus Ispėti vartotojus Kiek vartotoju priima masinius laiškus Kiek vartotojų priima laiškus Ne Taip Paprastas tekstas Ištaisykite klaidas ir bandykite dar kartą Sukurta  Peržiūra Gauti masinius laiškus Gauti ispėjimus arba prenumeratą Gavėjai Rolė Ieškoti Siųsti Siųsti žinutę grupėms Siųsti žinutę vartotojams Siųsti žinutę vartotojų grupėms Siųsti laišką atskiriems vartotojams Siųsti laišką vartotojų grupėms Siųsti %s Siųsti grupei Siųsti vartotojui Siuntėjas Tema Vartotojai, kurių išjungti ispėjimai čia nerodomi Spausk CTRL ir pasitink keletą Vartotojas Vartotojų nustatymai Statistika Prisijungimo vardas Taip Jums neleidžiama siųsti žinučių Jums leidžiama pasirinkti tik po vieną vartotoją Pasirinkite kelias grupes laikydami CTRL migtuką Galite pasirinkti kelis vartotojus žymėdami pele įspaudus CTRL migtuką Siųskite pranešimus pagal vartotojų roles Galite siųsti žinutes vienam ar keliems vartotojams pasirinktinai Įrašykite temą Pasirinkite gavėja Įrašykite žinutę Turite pasirinkti nors vieną rolę Nepasirinktas laiško formatas 