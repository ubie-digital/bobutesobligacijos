<?php if (!defined ('ABSPATH')) die ('Not allowed'); ?>
<div class="wrap">
	<h2><?php _e ('Question Groups', 'faqtastic'); ?></h2>
			<?php $this->submenu (true); ?>
			
	<?php if (count ($groups) > 0) : ?>
		<?php $this->render_admin ('pager', array ('pager' => $pager)); ?>
		
		<table class="widefat post fixed list">
			<thead>
				<tr>
					<th><?php echo $pager->sortable ('name', __('Name', 'faqtastic')) ?></th>
					<th><?php echo $pager->sortable ('page_id', __('Attached page', 'faqtastic')) ?></th>
					<th class="center"><?php echo $pager->sortable ('questions', 'Klausimai') ?></th>
					<th class="center"><?php _e('Pending','faqtastic'); ?></th>
					<th class="center" width="16"></th>
				</tr>
			</thead>

			<?php if ($pager->total_pages () > 1) : ?>
			<tfoot>
				<tr>
					<td colspan="5">
						<div class="options">
					<?php foreach ($pager->area_pages () AS $page) : ?>
						<?php echo $page ?>
					<?php endforeach; ?>
						</div>
					</td>
				</tr>
			</tfoot>
			<?php endif; ?>
			
			<tbody>
			<?php foreach ($groups AS $pos => $group) : ?>
			<tr id="group_<?php echo $group->id ?>"<?php if ($pos % 2 == 1) echo ' class="alt"' ?>>
				<?php $this->render_admin ('group_item', array ('group' => $group)); ?>
			</tr>
			<?php endforeach; ?>
			</tbody>

		</table>

	<?php else : ?>
		<p><?php _e ('There are no groups!', 'faqtastic'); ?></p>
	<?php endif; ?>
	
	<div id="loading" style="display: none">
		<img src="<?php echo $this->url () ?>/images/loading.gif" alt="<?php _e ('Loading', 'faqtastic'); ?>" width="32" height="32"/>
	</div>
</div>

<div class="wrap">
	<h2><?php _e ('Create Question Group', 'faqtastic'); ?></h2>
	<form action="<?php echo $this->url ($_SERVER['REQUEST_URI']) ?>" method="post" accept-charset="utf-8">
		<?php _e ('Group name', 'faqtastic'); ?>: <input type="text" name="group_name" value="" id="name"/> <?php _e ('Attached page', 'faqtastic'); ?>:
		<select name="page_id">
			<option value="0"><?php _e ('No page', 'faqtastic'); ?></option>
			<?php parent_dropdown ($group->page_id); ?>
		</select>
		
		<input class="button-secondary" type="submit" name="addgroup" value="<?php _e ('Add group', 'faqtastic'); ?>" id="group"/>
	</form>
</div>

<?php if (!empty ($groups)) : ?>
<div class="wrap">
	<h2><?php _e ('Publish an FAQ Page', 'faqtastic'); ?></h2>
	<form action="<?php echo $this->url ($_SERVER['REQUEST_URI']) ?>" method="post" accept-charset="utf-8">
		<table width="100%">
			<tr>
				<th width="160" align="right" valign="middle"><?php _e ('Page name', 'faqtastic'); ?>:</th>
				<td colspan="2"><input type="text" size="40" name="page_name" value=""/></td>
			</tr>
			<tr>
				<th align="right" valign="middle"><?php _e ('Question Group', 'faqtastic'); ?>:</th>
				<td colspan="2">
					<select name="group">
						<?php foreach ($groups AS $group) : ?>
						<option value="<?php echo $group->id ?>"><?php echo htmlspecialchars ($group->name) ?></option>
						<?php endforeach; ?>
					</select>
				</td>
			</tr>
			<?php if (FAQ_Features::is_pro ()) : ?>
				<?php $this->features->render_admin ('group_page_publish', array ('post' => $this->url ($_SERVER['REQUEST_URI']))); ?>
			<?php endif; ?>
			<tr>
				<th></th>
				<td><input class="button-secondary" type="submit" name="publish" value="<?php _e( 'Publish', 'faqtastic'); ?>"/></td>
			</tr>
		</table>
	</form>
</div>
<?php endif; ?>