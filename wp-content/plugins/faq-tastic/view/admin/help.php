<?php if (!defined ('ABSPATH')) die ('Not allowed'); ?>
<div class="wrap">
	<h2><?php _e ('FAQ-Tastic Help (Quick Reference)', 'faqtastic'); ?></h2>
		<?php $this->submenu (true); ?>

	<h2><?php _e ('Displaying an FAQ on your site', 'faqtastic'); ?></h2>
	<p><code>[faq list <strong>pavadinimas</strong>]</code></p>
	
	<p><?php _e ('Where <code><strong>name</strong></code> is the name of a Question Group.', 'faqtastic'); ?></p>
	
	<h2><?php _e ('Displaying "Jump Links" (anchor links) on your site', 'faqtastic'); ?></h2>
	<p><code>[faq summary <strong>pavadinimas</strong>]</code></p>
	
	<p><?php _e ('Where <code><strong>name</strong></code> is the name of a Question Group.', 'faqtastic'); ?></p>
	

	<h2><?php _e ('Allowing visitors to ask their questions', 'faqtastic'); ?></h2>

	<p><code>[faq ask <strong>pavadinimas</strong>]</code></p>
	
	<p><?php _e ('Where <code><strong>name</strong></code> is the name of a Question Group.', 'faqtastic'); ?></p>
</div>