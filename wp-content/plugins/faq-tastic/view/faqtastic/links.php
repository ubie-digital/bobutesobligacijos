<?php if (!defined ('ABSPATH')) die ('Not allowed'); ?><?php if (!empty ($questions)) :?>

    <div class="accordion">
        <dl>
		<?php foreach ($questions AS $pos => $question) : ?>
            <dt><a class="accordionTitle" href="#"><?php echo $question->question; ?></a></dt>
            <dd class="accordionItem accordionItemCollapsed">
                <p><?php echo nl2br($question->answer); ?></p>
            </dd>
		<?php endforeach; ?>
        </dl>
    </div>


    <script type="text/javascript">

        ( function( window ) {

            'use strict';

            function classReg( className ) {
                return new RegExp("(^|\\s+)" + className + "(\\s+|$)");
            }

            var hasClass, addClass, removeClass;

            if ( 'classList' in document.documentElement ) {
                hasClass = function( elem, c ) {
                    return elem.classList.contains( c );
                };
                addClass = function( elem, c ) {
                    elem.classList.add( c );
                };
                removeClass = function( elem, c ) {
                    elem.classList.remove( c );
                };
            }
            else {
                hasClass = function( elem, c ) {
                    return classReg( c ).test( elem.className );
                };
                addClass = function( elem, c ) {
                    if ( !hasClass( elem, c ) ) {
                        elem.className = elem.className + ' ' + c;
                    }
                };
                removeClass = function( elem, c ) {
                    elem.className = elem.className.replace( classReg( c ), ' ' );
                };
            }

            function toggleClass( elem, c ) {
                var fn = hasClass( elem, c ) ? removeClass : addClass;
                fn( elem, c );
            }

            var classie = {
                // full names
                hasClass: hasClass,
                addClass: addClass,
                removeClass: removeClass,
                toggleClass: toggleClass,
                // short names
                has: hasClass,
                add: addClass,
                remove: removeClass,
                toggle: toggleClass
            };

// transport
            if ( typeof define === 'function' && define.amd ) {
                // AMD
                define( classie );
            } else {
                // browser global
                window.classie = classie;
            }

        })( window );

        //fake jQuery
        var $ = function(selector){
            return document.querySelector(selector);
        }
        var accordion = $('.accordion');


        //add event listener to all anchor tags with accordion title class
        accordion.addEventListener("click",function(e) {
            e.stopPropagation();
            e.preventDefault();
            if(e.target && e.target.nodeName == "A") {
                var classes = e.target.className.split(" ");
                if(classes) {
                    for(var x = 0; x < classes.length; x++) {
                        if(classes[x] == "accordionTitle") {
                            var title = e.target;

                            //next element sibling needs to be tested in IE8+ for any crashing problems
                            var content = e.target.parentNode.nextElementSibling;

                            //use classie to then toggle the active class which will then open and close the accordion

                            classie.toggle(title, 'accordionTitleActive');
                            //this is just here to allow a custom animation to treat the content
                            //remove or add the collapsed state
                            classie.toggle(content, 'accordionItemCollapsed');

                        }
                    }
                }

            }
        });

    </script>


<?php else : ?>
  <p><?php _e ('There are no questions!', 'faqtastic'); ?></p>
<?php endif; ?>