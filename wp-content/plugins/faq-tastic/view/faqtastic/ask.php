<?php if (!defined ('ABSPATH')){ die ('Not allowed');  }
global $post;
$wp_session = WP_Session::get_instance();
?>
<br/>
<form action="<?php bloginfo ('wpurl') ?>/wp-content/plugins/faq-tastic/add-question.php" method="post" accept-charset="utf-8">
	<table width="100%">
		<tr>
			<th align="right" width="70" valign="top"><?php _e ('Question', 'faqtastic'); ?>:</th>
			<td align="left">
				<textarea name="faq_question" style="width: 100%" rows="5" cols="40"><?php if(isset($wp_session['postQuestionData']) && !empty($wp_session['postQuestionData'])) { echo $wp_session['postQuestionData']['faq_question']; } ?></textarea>
			</td>
		</tr>
		<tr>
			<th valign="top" align="right" width="70"><?php _e ('Email', 'faqtastic'); ?>:</th>
			<td align="left"><input type="text" size="30" name="faq_email" value="<?php if(isset($wp_session['postQuestionData']) && !empty($wp_session['postQuestionData'])) { echo $wp_session['postQuestionData']['faq_email']; } ?>"/>
				</td>
		</tr>
		<tr>
			<th width="70"></th>
			<td align="left"><input class="default_button_class button_ask_duk askPopUp" type="submit" name="add" value="<?php echo $group->ask_button () ?>"/></td>
		</tr>
	</table>
	<input type="hidden" name="group" value="<?php echo $group->id ?>"/>
	<input type="hidden" name="source" value="<?php echo get_permalink ($post->ID) ?>"/>
</form>
<br/>
