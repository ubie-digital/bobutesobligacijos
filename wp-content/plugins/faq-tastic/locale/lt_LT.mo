��    V      �     |      x     y     �  	   �  
   �     �  (   �  1   �               :     B     Z     s     �     �     �     �     �     �  H   �     	     %	     2	     9	     G	  3   W	     �	     �	     �	     �	     �	  
   �	  !   �	     	
     
     0
     ?
     O
  
   R
     ]
     b
     j
     s
     x
     �
     �
     �
  	   �
     �
     �
     �
     �
     �
     �
     �
               #     2     H     _     f     u     |     �     �  
   �     �     �  )   �     �  0   �  
   /     :     G     f     }     �     �  I   �  #     %   *     P     m  
   }  �  �     Y     s     �     �     �     �     �  	   �  !   �          +     G     g     o     w          �     �  	   �  F   �               1     8     G     W     l     x     �     �     �     �     �     �     �     �                     4     <  	   E     O  	   [     e     z  
   �     �     �     �     �  	   �     �     �  	             +     =     R     h  	   }     �     �     �  
   �     �     �     �     �  *   �          )  	   :  	   D  )   N     x     �     �     �  O   �  "   	  &   ,     S     i     y               ,   D          9   G   )   P   V   B   M              !          ?          @   K                        $           5   (                         H   "      <         O   -   ;      3   =          J   :       L   0   6      2                    1   R   %       N   E   C                           &                 	                  
      >       U       T       7          '   .       8       F   A       +      Q                  #   I   S   4      *   /        Add New Question Add Question Add group Admin role Allow ratings Allowing visitors to ask their questions An answer is needed before a question is approved Answer Approval or rejection message Approve Approved paged question Approved simple question Ask Question Ask question Asked by Attached page Author email Author email: Cancel Click on the links below to answer, approve or reject pending questions. Create Question Group Decide later Delete Delete Group? Delete selected Displaying "Jump Links" (anchor links) on your site Displaying an FAQ on your site Editing Question Email Email Options FAQ Answer:  FAQ-Tastic FAQ-Tastic Help (Quick Reference) FAQ-Tastic Options FAQ-Tastic Question Failed message General Options Go Group name Help Message Messages Name Negative Negative rating No page Options Page name Pending Pending Questions Per page Positive Positive rating Publish an FAQ Page Question Question Group Question Groups Question order Question submission:  Questions &amp; Groups Rating Rating message Reject Rejected question Save Search Select all Send email from Special Tags Text added to approval or rejection email Thank-you message The URL of the answer (only for paged questions) The answer The question There are no pending questions There are no questions There are no questions! This answer is bad This answer is good Where <code><strong>name</strong></code> is the name of a Question Group. You can approve this question here: You must supply a unique name and URL Your options have been saved delete selected select all Project-Id-Version: FAQ-Tastic 1.0.12
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2009-06-02 21:49-0000
PO-Revision-Date: 2014-10-27 10:35+0300
Last-Translator: Zain <webmaster@knowledgeconstructs.com>
Language-Team: FAQ-Tastic <translations@faq-tastic.com>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-KeywordsList: __;_e
X-Poedit-Basepath: .
Language: en_GB
X-Generator: Poedit 1.5.4
X-Poedit-SearchPath-0: ..
 Pridėti naują klausimą Pridėti klausimą Pridėti grupę Administratorius Leisti reitingavimą Vartotojų klausimų pateikimas Atsakymas yra būtinas Atsakymas Patvirtinimo ar atmetimo žinutė Patvirtinti Atsakyto puslapio klausimą Atsakyta į paprastą klausimą Klausti Klausti Klausė Puslapis Autoriaus el. paštas Autoriaus el. paštas Atšaukti Spausk ant nuorodos, kad atsakytum, patvirtintum ar atmestum klausymys Sukurti klausimų grupę Nuspręsti vėliau Trinti Trinti grupę? Ištrinti visus Nuorodos vaizdavimas Vaizdavimas Klausimo redagavimas El. paštas Pašto nustatymai Atsakymas į klausimą:  DUK DUK pagalba DUK nustatymai DUK klausimas Klaida žinutė Pagrindiniai nustatymai Pirmyn Grupės pavadinimas Pagalba Žinutė Žinutės Pavadinimas Neigiamas Neigiamas vertinimas Be puslapio Nustatymai Puslapio pavadinimas Patvirtinimas Laukiantys klausimai Rodyti puslapyje Teigiamas Teigiamas vertinimas Publikuoti DUK puslapį Klausymas Klausimo grupė Klausimų grupės Klausimų rikiavimas Klausimas nusiųstas: Klausimai ir Grupės Reitingas Įvertinimo žinutė Atmesti Klausimas atmestas Išsaugoti Paieška Pasirinkti visus Siūsti iš Tagai Šis tekstas bus pridėtas prie el. pašto Ačiū žinutė Atsakymo nuoroda Atsakymas Klausimas Nėra laukiančiū patvirtinimo klausimų Klausimų nėra Nėra klausimų Blogas atsakymas Geras atsakymas Kur <code><strong>pavadinimas</strong></code> yra klausimų grupės pavadinimas Galite patvirtinti klausimą čia: Privalote įvesti unikalų pavadinimą Nustatymai išsaugoti Ištrinti visus Pasirinkti visus 