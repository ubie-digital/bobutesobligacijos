<?php
/**
 * @package Prisijungimas tik su kodais
 * @version 2
 */
/*
Plugin Name: Turinio matymas tik su kodai
Plugin URI: http://mediainovacijos.lt
Description: Rodomas tinklapis tik su kodais
Author: Media Inovacijos
Version: 2
Author URI: http://mediainovacijos.lt
*/

add_action( 'parse_request', 'dmk_redirect_to_login_if_not_logged_in', 1 );
/**
 * Redirects a user to the login page if not logged in.
 *
 * @author Media Inovacijos
 */
function dmk_redirect_to_login_if_not_logged_in() {
    $wp_session = WP_Session::get_instance();
    $isCodeCorrect = $wp_session['myCodeCorrect'];
    if(!$isCodeCorrect && !is_admin() && !is_user_logged_in() && basename($_SERVER['PHP_SELF']) != 'wp-signup.php'){
        wp_redirect(get_site_url(). '/prisijungimas/', 301 ); exit;
    }
}


add_filter( 'login_url', 'dmk_strip_loggedout', 1, 1 );
/**
 * Strips '?loggedout=true' from redirect url after login.
 *
 * @author Media Inovacijos
 *
 * @param  string $login_url
 * @return string $login_url
 */
function dmk_strip_loggedout( $login_url ) {
	return str_replace( '%3Floggedout%3Dtrue', '', $login_url );
}
