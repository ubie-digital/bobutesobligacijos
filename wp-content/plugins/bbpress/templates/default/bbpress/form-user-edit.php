<?php

/**
 * bbPress User Profile Edit Part
 *
 * @package bbPress
 * @subpackage Theme
 */
global $wpdb, $wpdb_data_table;

if (isset($_POST['user_id'])) {
    $get_user_id = $_POST['user_id'];
    if (!current_user_can('edit_user', $get_user_id))
        return;
} else {
    if (!isset($user_ID))
        return;
    $get_user_id = $user_ID;
}

$get_user_id = intval($get_user_id);
$ef_db = $wpdb->get_results("SELECT FIELD_ID, VALUE FROM ".$wpdb_data_table." WHERE USER_ID = ".$get_user_id, ARRAY_A);
?>

<form id="bbp-your-profile" class="bbp-your-profile" action="<?php bbp_user_profile_edit_url( bbp_get_displayed_user_id() ); ?>" method="post" enctype="multipart/form-data">

	<h2 class="entry-title"><?php _e( 'Contact Info', 'bbpress' ) ?></h2>

	<?php do_action( 'bbp_user_edit_before' ); ?>

	<fieldset class="bbp-form">
		<legend><?php _e( 'Contact Info', 'bbpress' ) ?></legend>

		<?php do_action( 'bbp_user_edit_before_name' ); ?>

        <table class="profileEditTable noBorder">
            <tr>
                <th>

                    <div>
                        <label for="first_name"><?php _e( 'First Name', 'bbpress' ) ?></label>
                        <input type="text" name="first_name" id="first_name" value="<?php bbp_displayed_user_field( 'first_name', 'edit' ); ?>" class="regular-text" tabindex="<?php bbp_tab_index(); ?>" />
                    </div>

                    <div>
                        <label for="email"><?php _e( 'Email', 'bbpress' ); ?></label>

                        <input type="text" name="email" id="email" value="<?php bbp_displayed_user_field( 'user_email', 'edit' ); ?>" class="regular-text" tabindex="<?php bbp_tab_index(); ?>" />

                        <?php

                        // Handle address change requests
                        $new_email = get_option( bbp_get_displayed_user_id() . '_new_email' );
                        if ( !empty( $new_email ) && $new_email !== bbp_get_displayed_user_field( 'user_email', 'edit' ) ) : ?>

                            <span class="updated inline">

					<?php printf( __( 'There is a pending email address change to <code>%1$s</code>. <a href="%2$s">Cancel</a>', 'bbpress' ), $new_email['newemail'], esc_url( self_admin_url( 'user.php?dismiss=' . bbp_get_current_user_id()  . '_new_email' ) ) ); ?>

				</span>

                        <?php endif; ?>

                    </div>

                </th>
                <th>

                    <div>
                        <label for="last_name"><?php _e( 'Last Name', 'bbpress' ) ?></label>
                        <input type="text" name="last_name" id="last_name" value="<?php bbp_displayed_user_field( 'last_name', 'edit' ); ?>" class="regular-text" tabindex="<?php bbp_tab_index(); ?>" />
                    </div>

                    <div>
                        <label for="cimy_uef_TELEFONAS"><?php _e( 'Telefono nr.', 'bbpress' ) ?></label>
                        <input type="hidden" name="cimy_uef_TELEFONAS_1_prev_value" value="<?php echo $ef_db[0]['VALUE']; ?>">
                        <input style="width: 35%; display: inline-block" disabled="disabled" id="phonePrefix" class="regular-text" name="phonePrefix" type="text" maxlength="4" value="+370">
                        <input style="width: 62%; display: inline-block" id="cimy_uef_1" class="regular-text" name="cimy_uef_TELEFONAS" type="text" maxlength="8" value="<?php echo $ef_db[0]['VALUE']; ?>">
                    </div>

                </th>
            </tr>
        </table>

<hr/>
        <h2 class="entry-title">Pirkėjo informacija</h2>

        <div>
            <label for="cimy_uef_SUMA"><?php _e( 'Planuojama investuoti suma', 'bbpress' ); ?>:</label>
            <input type="hidden" name="cimy_uef_SUMA_3_prev_value" value="<?php echo $ef_db[2]['VALUE']; ?>">
            <input id="cimy_uef_3" class="regular-text width60proc" name="cimy_uef_SUMA" type="text" value="<?php echo $ef_db[2]['VALUE']; ?>" maxlength="10" tabindex="<?php bbp_tab_index(); ?>">
        </div>
<hr/>
        <div>
            <label class="userEditFieldText" for="reg-time">Registracijos laikas:</label>
            <?php bbp_displayed_user_field( 'user_registered', 'edit' ); ?>
        </div>

        <div>
            <label class="userEditFieldText" for="reg">Registracijos numeris:</label>
            INV-<?php echo $get_user_id; ?>
        </div>

        <hr/>

        <h2 class="entry-title">Prisijungimo duomenys</h2>

    <fieldset class="bbp-form">
        <legend>Prisijungimo duomenys</legend>

        <?php do_action( 'bbp_user_edit_before_account' ); ?>

        <div>
            <label for="user_login"><?php _e( 'Username', 'bbpress' ); ?></label>
            <input type="text" name="user_login" id="user_login" value="<?php bbp_displayed_user_field( 'user_login', 'edit' ); ?>" disabled="disabled" class="regular-text width60proc" tabindex="<?php bbp_tab_index(); ?>" />
        </div>

        <div>
            <label for="display_name"><?php _e( 'Display Name', 'bbpress' ) ?></label>

            <?php bbp_edit_user_display_name(); ?>

        </div>

        <div id="password">
            <label for="pass1"><?php _e( 'New Password', 'bbpress' ); ?></label>
            <fieldset class="bbp-form password">
                <input type="password" name="pass1" id="pass1" size="16" value="" autocomplete="off" tabindex="<?php bbp_tab_index(); ?>" />
                <span class="description"><?php _e( 'If you would like to change the password type a new one. Otherwise leave this blank.', 'bbpress' ); ?></span>

                <input type="password" name="pass2" id="pass2" size="16" value="" autocomplete="off" tabindex="<?php bbp_tab_index(); ?>" />
                <span class="description"><?php _e( 'Type your new password again.', 'bbpress' ); ?></span><br />
                <div id="pass-strength-result"></div>
                <span class="description indicator-hint"><?php _e( 'Your password should be at least ten characters long. Use upper and lower case letters, numbers, and symbols to make it even stronger.', 'bbpress' ); ?></span>
            </fieldset>
        </div>
        <?php do_action( 'bbp_user_edit_after_account' ); ?>

    </fieldset>
        <input style="display: none" type="text" name="cimy_uef_TAISYKLES" id="cimy_uef_2" value="1">


        <?php if ( current_user_can( 'edit_users' ) && ! bbp_is_user_home_edit() ) : ?>

        <h2 class="entry-title"><?php _e( 'User Role', 'bbpress' ) ?></h2>

        <fieldset class="bbp-form">
            <legend><?php _e( 'User Role', 'bbpress' ); ?></legend>

            <?php do_action( 'bbp_user_edit_before_role' ); ?>

            <?php if ( is_multisite() && is_super_admin() && current_user_can( 'manage_network_options' ) ) : ?>

                <div>
                    <label for="super_admin"><?php _e( 'Network Role', 'bbpress' ); ?></label>
                    <label>
                        <input class="checkbox" type="checkbox" id="super_admin" name="super_admin"<?php checked( is_super_admin( bbp_get_displayed_user_id() ) ); ?> tabindex="<?php bbp_tab_index(); ?>" />
                        <?php _e( 'Grant this user super admin privileges for the Network.', 'bbpress' ); ?>
                    </label>
                </div>

            <?php endif; ?>

            <?php bbp_get_template_part( 'form', 'user-roles' ); ?>

            <?php do_action( 'bbp_user_edit_after_role' ); ?>

        </fieldset>

    <?php endif; ?>

	<fieldset class="bbp-form">

		<?php do_action( 'bbp_user_edit_before_contact' ); ?>

		<?php foreach ( bbp_edit_user_contact_methods() as $name => $desc ) : ?>

			<div>
				<label for="<?php echo esc_attr( $name ); ?>"><?php echo apply_filters( 'user_' . $name . '_label', $desc ); ?></label>
				<input type="text" name="<?php echo esc_attr( $name ); ?>" id="<?php echo esc_attr( $name ); ?>" value="<?php bbp_displayed_user_field( $name, 'edit' ); ?>" class="regular-text" tabindex="<?php bbp_tab_index(); ?>" />
			</div>


		<?php endforeach; ?>

		<?php do_action( 'bbp_user_edit_after_contact' ); ?>

	</fieldset>
	<?php
    do_action( 'bbp_email_settings' );
    //do_action( 'bbp_user_edit_after' ); ?>
        <div style="display: none" id="userAvatar" class="userAvatar">
            <?php do_action('bbp_user_edit_after_about'); ?>
        </div>
	<fieldset class="submit">
		<legend><?php _e( 'Save Changes', 'bbpress' ); ?></legend>
		<div>

			<?php bbp_edit_user_form_fields(); ?>

			<button type="submit" tabindex="<?php bbp_tab_index(); ?>" id="bbp_user_edit_submit" name="bbp_user_edit_submit" class="button submit user-submit updateCommonButton"><?php bbp_is_user_home_edit() ? _e( 'Update Profile', 'bbpress' ) : _e( 'Update User', 'bbpress' ); ?></button>
		</div>
    </fieldset>
</form>