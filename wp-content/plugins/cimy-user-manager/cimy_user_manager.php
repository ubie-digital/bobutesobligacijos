<?php
/*
Plugin Name: Cimy User Manager
Plugin URI: http://www.marcocimmino.net/cimy-wordpress-plugins/cimy-user-manager/
Description: Import and export users from/to CSV files, supports all WordPress profile data also Cimy User Extra Fields plug-in
Version: 1.4.6
Author: Marco Cimmino
Author URI: mailto:cimmino.marco@gmail.com
*/

/*

Cimy User Manager - Import and export users from/to CSV files
Copyright (c) 2007-2012 Marco Cimmino

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.


The full copy of the GNU General Public License is available here: http://www.gnu.org/licenses/gpl.txt

*/

// pre 2.6 compatibility or if not defined
if (!defined("WP_CONTENT_DIR"))
	define("WP_CONTENT_DIR", ABSPATH."/wp_content");

$cum_plugin_path = plugin_basename(dirname(__FILE__))."/";
$cum_upload_path = WP_CONTENT_DIR."/cimy-user-manager/";

$cimy_um_domain = 'cimy_um';
$cimy_um_i18n_is_setup = false;
cimy_um_i18n_setup();

// function that add the submenu under 'Users'
if (is_network_admin())
	add_action('network_admin_menu', 'cimy_um_admin_menu_custom');
else
	add_action('admin_menu', 'cimy_um_admin_menu_custom');

$userid_code = 'ID';
$useremail_code = 'El. paštas';
$username_code = 'Prisijungimo vardas';
$firstname_code = 'Vardas';
$lastname_code = 'Pavardė';
$password_code = 'Slaptažodžio kodas';
$role_code = "Rolė";
$registered_code = "Užsiregistravo";
$displayname_code = "Rodomas vardas";
$usernicename_code = "Slapyvardis";

add_action('admin_init', 'cimy_um_download_database');

function cimy_um_download_database() {
	global $cum_upload_path;
	if (!empty($_POST["cimy_um_filename"])) {
		if (strpos($_SERVER['HTTP_REFERER'], admin_url('users.php?page=cimy_user_manager')) !== false) {
			// not whom we are expecting? exit!
			if (!check_admin_referer('cimy_um_download', 'cimy_um_downloadnonce'))
				return;
			$cimy_um_filename = $_POST["cimy_um_filename"];
			// sanitize the file name
			$cimy_um_filename = sanitize_file_name($cimy_um_filename);
			$cimy_um_fullpath_file = $cum_upload_path.$cimy_um_filename;
			// does not exist? exit!
			if (!is_file($cimy_um_fullpath_file))
				return;

			header("Pragma: "); // Leave blank for issues with IE
			header("Expires: 0");
			header('Vary: User-Agent');
			header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
			header("Content-Type: text/csv");
			header("Content-Type: application/force-download");
			header("Content-Type: application/download");
			header("Content-Disposition: attachment; filename=\"".esc_html($cimy_um_filename)."\";"); // cannot use esc_url any more because prepends 'http' (doh)
			header("Content-Transfer-Encoding: binary");
			header("Content-Length: ".filesize($cimy_um_fullpath_file));
			readfile($cimy_um_fullpath_file);
			exit();
		}
	}
}

function cimy_um_i18n_setup() {
	global $cimy_um_domain, $cimy_um_i18n_is_setup, $cum_plugin_path;

	if ($cimy_um_i18n_is_setup)
		return;

	load_plugin_textdomain($cimy_um_domain, false, $cum_plugin_path.'langs');
	$cimy_um_i18n_is_setup = true;
}


function cimy_um_admin_menu_custom() {
	global $cimy_um_domain;

	add_submenu_page('users.php', __('Cimy User Manager', $cimy_um_domain), __('Cimy User Manager', $cimy_um_domain), "list_users", "cimy_user_manager", 'cimy_um_import_export_page');
}

function cimy_um_import_export_page() {
	global $cimy_um_domain, $cimy_uef_name, $cum_upload_path;

	if (!current_user_can('list_users'))
		return;

	if (!empty($_POST))
		if (!check_admin_referer('cimy_um_importexport', 'cimy_um_importexportnonce'))
			return;

	$results_export = array();

	if (isset($_POST['cimy_um_export']))
		$results_export = cimy_um_export_data();
	
	if (isset($_POST['db_date_format']))
		$db_date_format = esc_attr($_POST['db_date_format']);
	else
		$db_date_format = "%d %B %Y @%H:%M";
	
	if (!isset($cimy_uef_name)) {
		$db_extra_fields_warning = "<br /><strong>".__("You must activate Cimy User Extra Fields to export data from that plug-in", $cimy_um_domain)."</strong>";
	}
	else
		$db_extra_fields_warning = "";
	
	if (isset($_POST["db_field_separator"]))
		$field_separator = stripslashes($_POST["db_field_separator"]);
	else
		$field_separator = ",";

	if (isset($_POST["db_text_separator"]))
		$text_separator = stripslashes($_POST["db_text_separator"]);
	else
		$text_separator = "\"";
	$use_fput_csv = ((strlen($field_separator) == 1) && (strlen($text_separator) == 1) && version_compare(PHP_VERSION, "5.1.0", '>=')) ? true : false;
	?>
	<div class="wrap" id="options">
	<?php
		if (function_exists("screen_icon"))
			screen_icon("users");
	?>
	<h2><?php _e("Export Users", $cimy_um_domain); ?></h2><?php
	
	// print successes/errors if there are some
	if (count($results_export) > 0) {
	?>
		<br /><div class="updated">
	<?php
		if (isset($results_export["tmp_file"])) {
			echo "<h3>".__("FILE GENERATED", $cimy_um_domain)." (".count($results_export["tmp_file"]).")</h3>";
			echo $results_export["tmp_file"];
			echo "<form name=\"cimy_um_download\" id=\"cimy_um_download\" method=\"post\"><input type=\"hidden\" name=\"cimy_um_filename\" value=\"".basename($results_export["tmp_file"])."\" /><input type=\"submit\" value=\"".__("Download Export File")."\" />";
			wp_nonce_field('cimy_um_download', 'cimy_um_downloadnonce', false);
			echo "</form>";
			echo "<br />";
		}
		
		if (isset($results_export["error"])) {
			echo "<h3>".__("ERRORS", $cimy_um_domain)." (".count($results_export["error"]).")</h3>";
	
			foreach ($results_export["error"] as $result)
				echo $result."<br />";
		}
		
		if (isset($results_export["exported"])) {
			echo "<h3>".__("USERS SUCCESSFULLY EXPORTED", $cimy_um_domain)." (".count($results_export["exported"]).")</h3>";
	
			foreach ($results_export["exported"] as $result)
				echo $result."<br />";
		}
		?><br /></div><?php
	}
	if ((!is_dir($cum_upload_path)) && (is_writable(WP_CONTENT_DIR))) {
		if (defined("FS_CHMOD_DIR"))
			@mkdir($cum_upload_path, FS_CHMOD_DIR);
		else
			wp_mkdir_p($cum_upload_path);
	}
	if (is_writable($cum_upload_path) && (!file_exists($cum_upload_path.".htaccess")))
		cimy_um_create_htaccess();
	?>
	
	<p>
	</p>
	
	<form name="cimy_um_export" id="cimy_um_export" method="post">
	<?php wp_nonce_field('cimy_um_importexport', 'cimy_um_importexportnonce', false); ?>
	<table class="form-table">
		<tr>
			<th scope="row" width="40%"><?php _e("Upload path", $cimy_um_domain); ?></th>
			<td width="60%">
			<?php
				if (is_writable($cum_upload_path))
					echo "<em>".$cum_upload_path."</em><br />".__("is created and writable", $cimy_um_domain);
				else
					echo "<em>".$cum_upload_path."</em><br />".__("is NOT created or webserver does NOT have permission to write on it", $cimy_um_domain);
			?>
			</td>
		</tr>
		<tr>
			<th scope="row"><?php _e("Select field delimiter", $cimy_um_domain); ?></th>
			<td>
				<input type="text" name="db_field_separator" value="<?php echo esc_attr($field_separator); ?>" />  <?php _e('If your CSV file is like: "field1","field2" then you need to use comma', $cimy_um_domain); ?>
			</td>
		</tr>
		<tr>
			<th scope="row"><?php _e("Select text delimiter", $cimy_um_domain); ?></th>
			<td>
				<input type="text" name="db_text_separator" value="<?php echo esc_attr($text_separator); ?>" />  <?php _e('If your CSV file is like: "field1","field2" then you need to use double quote', $cimy_um_domain); ?>
			</td>
		</tr>
		<tr>
			<th scope="row"><?php _e("Sort by", $cimy_um_domain); ?></th>
			<td>
				<select name="db_sort_by">
                    <option value='invest_mount'<?php selected('invest_mount', $_POST['db_sort_by'], true); ?>>Investuojama suma</option>
                    <option value='registered'<?php selected('registered', $_POST['db_sort_by'], true); ?>><?php _e('Registered date', $cimy_um_domain); ?></option>
					<option value='login'<?php selected('login', $_POST['db_sort_by'], true); ?>><?php _e('Login Name', $cimy_um_domain); ?></option>
					<option value='email'<?php selected('email', $_POST['db_sort_by'], true); ?>><?php _e('Email'); ?></option>
					<option value='display_name'<?php selected('display_name', $_POST['db_sort_by'], true); ?>><?php _e('Display Name', $cimy_um_domain); ?></option>
					<option value='url'<?php selected('url', $_POST['db_sort_by'], true); ?>><?php _e('Website'); ?></option>
					<option value='post_count'<?php selected('post_count', $_POST['db_sort_by'], true); ?>><?php _e('Post Count', $cimy_um_domain); ?></option>
				</select>
			</td>
		</tr>
		<tr>
<?php
		if ($use_fput_csv) {
?>
			<th scope="row"><?php _e("Enable Excel compatibility mode", $cimy_um_domain); ?></th>
<?php
		}
		else {
?>
			<th scope="row"><?php _e("Use UTF-16LE encoding", $cimy_um_domain); ?></th>
<?php
		}
?>
			<td>
				<input type="checkbox" name="db_excel_compatibility" value="1" checked="checked" />  <?php _e("Select this option if you want more Microsoft Excel compatibility", $cimy_um_domain); ?>
			</td>
		</tr>
		<tr>
			<th scope="row"><?php _e("Add also Cimy User Extra Fields data", $cimy_um_domain); ?></th>
			<td>
				<input type="checkbox" name="db_extra_fields" value="1" checked="checked" />  <?php _e("Select this option to let the plug-in export also data present into Cimy User Extra Fields", $cimy_um_domain); echo $db_extra_fields_warning; ?>
			</td>
		</tr>
<!--
		<tr>
			<th><?php //_e("Select date format", $cimy_um_domain); ?></th>
			<td>
				<input type="text" name="db_date_format" value="<?php //echo $db_date_format; ?>" />  <?php //_e("Select the date/time format to represent registration dates (if any)", $cimy_um_domain); ?><br />
				<?php //_e("More info about date/time format are on this", $cimy_um_domain); ?> <a href="http://www.php.net/manual/en/function.strftime.php"><?php //_e("LINK", $cimy_um_domain); ?></a>
			</td>
		</tr>
-->
	</table>
	
	<input type="hidden" name="cimy_um_export" value="1" />
	<p class="submit"><input class="button-primary" type="submit" name="Export" value="<?php _e('Export') ?>" /></p>
	</form>
	</div>
	<br />
	<?php
}


function cimy_um_create_htaccess() {
	global $cum_upload_path;

	$file = $cum_upload_path.".htaccess";
	$fd_file = fopen($file, "w");

	$line = '<Files ~ ".*\..*">
order allow,deny
deny from all
</Files>';
	fwrite($fd_file, $line);
	fclose($fd_file);
}

function cimy_um_export_data() {
	global $wpdb, $wpdb_data_table, $wpdb_fields_table, $cimy_um_domain, $cum_upload_path;
	global $userid_code, $useremail_code, $username_code, $firstname_code, $lastname_code, $password_code, $role_code, $registered_code, $displayname_code, $usernicename_code, $cimy_uef_name;
	
	$results = array();
	
	if (!current_user_can('list_users'))
		return;
	
	set_time_limit(0);

	$field_separator = stripslashes($_POST["db_field_separator"]);
	$text_separator = stripslashes($_POST["db_text_separator"]);
	// fputcsv has been introduced with PHP v5.1.0 and works when delimiters are 1 character long
	$use_fput_csv = ((strlen($field_separator) == 1) && (strlen($text_separator) == 1) && version_compare(PHP_VERSION, "5.1.0", '>=')) ? true : false;

	if ((isset($_POST["db_extra_fields"])) && (isset($cimy_uef_name))) {
		global $wpdb_data_table;
		$extra_fields = get_cimyFields();
		$all_radio_fields = array();
	}
	else {
		$extra_fields = false;
	}
	
	if (isset($_POST['db_date_format']))
		$db_date_format = $_POST['db_date_format'];
	else
		$db_date_format = "";

	$tmpfile = $cum_upload_path."cimy_um_exported_users-".date("Ymd-His").".csv";
	$fd_tmp_file = fopen($tmpfile, "w");
	if ($use_fput_csv && !empty($_POST['db_excel_compatibility'])) {
		// http://www.skoumal.net/en/making-utf-8-csv-excel
		// add BOM to fix UTF-8 in Excel
		fputs($fd_tmp_file, chr(0xEF).chr(0xBB).chr(0xBF));
	}

	if ($use_fput_csv) {
		$header_array = array(
					$userid_code,
					$username_code,
					$role_code,
					$firstname_code,
					$lastname_code,
					$displayname_code,
					$usernicename_code,
					$useremail_code,
					$registered_code
				);
	}
	else {
		$line = $text_separator.$userid_code.$text_separator.
			$field_separator.$text_separator.$username_code.$text_separator.
			$field_separator.$text_separator.$role_code.$text_separator.
			$field_separator.$text_separator.$firstname_code.$text_separator.
			$field_separator.$text_separator.$lastname_code.$text_separator.
			$field_separator.$text_separator.$displayname_code.$text_separator.
			$field_separator.$text_separator.$usernicename_code.$text_separator.
			$field_separator.$text_separator.$useremail_code.$text_separator.
			$field_separator.$text_separator.$registered_code.$text_separator;
	}
	
	if ($extra_fields) {
		foreach ($extra_fields as $field) {
			// avoid radio fields duplicates
			if ($field["TYPE"] == "radio") {
				if (in_array($field["NAME"], $all_radio_fields))
					continue;
				else
					$all_radio_fields[] = $field["NAME"];
			}
			else if ($field["TYPE"] == "registration-date")
				continue;
			if ($use_fput_csv)
				$header_array[] = $field["NAME"];
			else
				$line .= $field_separator.$text_separator.$field["NAME"].$text_separator;
		}
	}

	if ($use_fput_csv) {
		fputcsv($fd_tmp_file, $header_array, $field_separator, $text_separator);
	}
	else {
		$line = str_replace(array("\r\n\r\n", "\r\n", "\n\r", "\r", "\n" ), " ", $line);
		$line .= "\r";

		// UTF-16LE is needed to open and use the csv-file in Excel (thanks to Jean-Pierre)
		// http://www.php.net/manual/en/function.iconv.php#104287
		// http://www.php.net/manual/en/function.fwrite.php#69566
		if (!empty($_POST['db_excel_compatibility']))
			$line = mb_convert_encoding($line, 'UTF-16LE', 'UTF-8');

		fwrite($fd_tmp_file, $line);
	}
	
	$results["exported"] = array();

	$offset = 0;
	// max number of users to be retrieved at once
	// bigger number increases speed, but also memory usage, be reasonable
	$limit = 250;

    $sortInvestAmount = false;
    $orderBy = $_POST['db_sort_by'];
    if($orderBy == 'invest_mount') {
        $sortInvestAmount = true;
        $orderBy = 'registered';
    }

	$args = array(
		'blog_id' => $GLOBALS['blog_id'],
		'role' => '',
		'meta_key' => '',
		'meta_value' => '',
		'meta_compare' => '',
		'include' => array(),
		'exclude' => array(),
		'orderby' => $orderBy,
		'order' => 'ASC',
		'offset' => $offset,
		'search' => '',
		'number' => $limit,
		'count_total' => true,
		'fields' => 'all_with_meta',
		'who' => ''
	);

	// needed otherwise does not export everything
	if (is_network_admin())
		$args['blog_id'] = 0;

	$all_users = get_users($args);
	$tot_users = count($all_users);

    $orderingUsers = array();

	while ($tot_users > 0) {
		foreach ($all_users as $current_user) {
			$results["exported"][] = $current_user->user_login;
			if ($use_fput_csv) {
				$field_array = array(
					$current_user->ID,
					$current_user->user_login,
					$current_user->roles[0],
					$current_user->first_name,
					$current_user->last_name,
					$current_user->display_name,
					$current_user->user_nicename,
					$current_user->user_email,
					$current_user->user_registered,
				);
			}
			else {
				$line = $text_separator.$current_user->ID.$text_separator.
					$field_separator.$text_separator.$current_user->user_login.$text_separator.
					$field_separator.$text_separator.$current_user->roles[0].$text_separator.
					$field_separator.$text_separator.$current_user->first_name.$text_separator.
					$field_separator.$text_separator.$current_user->last_name.$text_separator.
					$field_separator.$text_separator.$current_user->display_name.$text_separator.
					$field_separator.$text_separator.$current_user->user_nicename.$text_separator.
					$field_separator.$text_separator.$current_user->user_email.$text_separator.
					$field_separator.$text_separator.$current_user->user_registered.$text_separator;
			}

			if ($extra_fields) {
				$all_radio_fields = array();
				$ef_db = get_cimyFieldValue($current_user->ID, false);
				$i = 0;

				foreach ($extra_fields as $field) {
					$db_name = "";
					$db_value = "";

					// avoid radio fields duplicates
					if ($field["TYPE"] == "radio") {
						if (in_array($field["NAME"], $all_radio_fields))
							continue;
						else
							$all_radio_fields[] = $field["NAME"];
					}

					if (isset($ef_db[$i]))
						$db_name = $ef_db[$i]['NAME'];

					// can happen if the field's data has not been written in the DB yet
					// this issue has been introduced with the get_cimyFieldValue calls optimization in v1.1.0
					if ($field["NAME"] == $db_name) {
						if (isset($ef_db[$i]))
							$db_value = $ef_db[$i]['VALUE'];

						$i++;
						if ($field["TYPE"] == "registration-date")
							continue;
// 							$db_value = cimy_get_formatted_date($db_value, $db_date_format);
					}
					if ($use_fput_csv)
						$field_array[] = $db_value;
					else
						$line .= $field_separator.$text_separator.$db_value.$text_separator;
				}
			}

            $orderingUsers[] = $field_array;
		}

        if ($sortInvestAmount) {
            usort($orderingUsers, 'sortByOrder');
            $orderingUsers = array_reverse($orderingUsers);
        }

        foreach($orderingUsers as $orderinguser) {

            if ($use_fput_csv) {
                fputcsv($fd_tmp_file, $orderinguser, $field_separator, $text_separator);
            }
            else {
                $line = str_replace(array("\r\n\r\n", "\r\n", "\n\r", "\r", "\n" ), " ", $line);
                $line .= "\r";

                // UTF-16LE is needed to open and use the csv-file in Excel (thanks to Jean-Pierre)
                // http://www.php.net/manual/en/function.iconv.php#104287
                // http://www.php.net/manual/en/function.fwrite.php#69566
                if (!empty($_POST['db_excel_compatibility']))
                    $line = mb_convert_encoding($line, 'UTF-16LE', 'UTF-8');

                fwrite($fd_tmp_file, $line);
            }
        }
		// get next round of users (if any)
		$offset += $tot_users;
		$args["offset"] = $offset;
		$all_users = get_users($args);
		$tot_users = count($all_users);
	}
	
	fclose($fd_tmp_file);

	$results["tmp_file"] = $tmpfile;

	return $results;
}

function sortByOrder($a, $b) {
    return $a['13'] - $b['13'];
}
