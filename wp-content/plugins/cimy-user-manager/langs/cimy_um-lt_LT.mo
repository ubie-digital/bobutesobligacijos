��    $      <  5   \      0  	   1  $   ;     `     u     �     �     �     �     �     �     �     �     �  F     M   M  
   �  
   �     �     �     �     �  A     Z   D  :   �     �     �     �     
           (  C   ,     p  $   �     �     �  
  �     �  "   �     	     '	     >	     Q	     `	     t	     |	     �	     �	     �	     �	  7   �	  H   
     c
     w
     �
     �
     �
     �
  '   �
  a      >   b     �  !   �     �     �  	          ;        J  %   a     �     �                 #                                  $       "                      !                      
                   	                                                           (line %s) Add also Cimy User Extra Fields data Cannot open the file Cimy User Manager Create users Display Name Download Export File ERRORS Email Enable Excel compatibility mode Export Export Users FILE GENERATED If your CSV file is like: "field1","field2" then you need to use comma If your CSV file is like: "field1","field2" then you need to use double quote Login Name Post Count Registered date Select field delimiter Select text delimiter Select the CSV file Select this option if you want more Microsoft Excel compatibility Select this option to let the plug-in export also data present into Cimy User Extra Fields Send an email to the existing users with the new password. Sort by USERS SUCCESSFULLY EXPORTED Upload path Use UTF-16LE encoding Website and is NOT created or webserver does NOT have permission to write on it is created and writable userid '%s' is not present in the DB userid is missing username is missing Project-Id-Version: Cimy User Manager
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2013-06-02 16:48-0800
PO-Revision-Date: 2014-10-08 16:46+0300
Last-Translator: Marco Cimmino <cimmino.marco@gmail.com>
Language-Team:  <cimmino.marco@gmail.com>
Language: it_IT
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-KeywordsList: __;_e
X-Poedit-SourceCharset: utf-8
X-Poedit-Basepath: .
X-Generator: Poedit 1.5.4
X-Poedit-SearchPath-0: /var/www/wp-content/plugins/cimy-user-manager
 (eilutė %s) Pridėti visus papildomus duomenis Negalima atidaryti failo Duomenų eksportavimas Sukurti vartotojus Rodomas vardas Parsisiųsti failą KLAIDOS El. pašto adresas Pritaikyti EXEL programai Eksportavimas Eksportuoti vartotojus FAILAS SUGENERUOTAS Jei CSV: "field1","field2" tada reikia naudoti kablelį Jeigu jūsų CSV: "field1","field2" tada reikia naudoti dvigumas kabutes Prisijungimo vardas Įrašų skaičius Registracijos data Laukų atskirimas Teksto atskirimas Pasirinkite CSV failą Jei atidarysite failą MS EXEL programa Pasirinkite eksportuoti tokiems duomenims kaip tel. numeriai, investuojama suma, pastabos ir t.t. Inviare una email agli utenti esistenti con la nuova password. Rikiuoti pagal VARTOTOJAI SĖKMINGAI EKSPORTUOTI Failų kaupimo vieta Naudoti UTF-16LE koduotę Tinklapis ir nesukurtas arba serveris neturi reikiamų įrašymo teisių sukurtas ir įrašomas vartotojo su ID '%s' nėra duombazėj nėra vartotojo ID trūksta prisijungimo vardo 