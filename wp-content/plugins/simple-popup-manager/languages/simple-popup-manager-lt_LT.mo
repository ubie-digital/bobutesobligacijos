��          �      �           	     #     @  �   N     "  2   =     p     �     �      �  >   �  *     
   <     G     X     g  (   w     �     �     �  5   �       �   5     ,  $   :     _     s     u  6   ~     �  	   �     �     �  ,         -     B  
   T     _     y  %   �     �  :   �     �          &                         
                                                                    	           Background screen color : Cookie duration (in days) :  Debug mode :  Dimension will set width and height of the popup. Threshold is the limit in pixel under which popup won't show up despite all others settings. 480 is the default value to exclude small devices like mobile phone. Dimensions and threshold : Disable closing popup when clicking outside it  :  Display close button :  Enable plugin :  Every where on the site Height of the popup in pixels :  No cookie, popup only visible for logged in admin on frontpage Only on the home or front page of the site Opacity :  Popup Content :  Popup Settings Possible values Should be hexadecimal color like #000000 Simple Pop Manager Simple Pop Manager description Threshold :  Where would you like the PopUp window to be visible ? Width of the popup in pixels :  Project-Id-Version: Simple popup manager
POT-Creation-Date: 
PO-Revision-Date: 
Last-Translator: 
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: fr_FR
X-Generator: Poedit 1.5.4
 Fono spalva:  Sausainėlių galiojimas (dienomis): Testavimo režimas:   Matmenys Nebeleisti uždaryti paspaudus už pranešimo ribų :  Rodyti uždarymo migtuką: Įjungti: Visur tinklapyje Aukštis pikseliais: Rodoma tik prisijungus kaip administratoriui Pradiniame puslapyje Nepermatomumas :  Turinys :  Pasisveikinimo nustatymai dienos Geriausiai matomas tekstas su #000000 Pasisveikinimas Pranešimas rodomas atėjus į tinklapį naujam vartotojui Nerodyti jeigu mažiau nei:  Kur rodyti pranešimą? Plotis pikseliais: 